export const state = () => ({
    isLoading: false,
})

export const mutations = {
    start(state) {
        state.isLoading = true
    },
    stop(state) {
        state.isLoading = false
    }
}