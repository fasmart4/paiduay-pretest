import { firebaseAuth } from '~/plugins/firebaseInstance'

export default async () => {
    const user = firebaseAuth.currentUser
    if (user != null) {
        //signout
        await firebaseAuth.signOut()
        console.log('SignOUT')
        return
    }
}