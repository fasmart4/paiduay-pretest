export default ({ redirect, route }) => {
    if(route.path === '/') {
        return redirect('/signin')
    }
}