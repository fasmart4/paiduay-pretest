import { firebaseAuth } from '~/plugins/firebaseInstance'

const pathWithOutLogin = [
    '/signin',
    '/signup',
]

export default ({ redirect, route }) => {

    return firebaseAuth.onAuthStateChanged((user) => {
        if(!user && !pathWithOutLogin.includes(route.path)) {
            return redirect('/signin')
        }
    })

}