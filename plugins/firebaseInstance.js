import * as firebase from 'firebase'
import 'firebase/auth'
import 'firebase/firestore'
import firebaseConfig from '~/configs/firebase'

// Initialize Firebase
firebase.initializeApp(firebaseConfig)
firebase.analytics()

export const firebaseAuth = firebase.auth()
export const firebaseDb = firebase.firestore()


