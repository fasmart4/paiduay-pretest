import Vue from 'vue'
import { ValidationProvider, ValidationObserver, extend } from 'vee-validate'

// extend rule here
import * as rules from 'vee-validate/dist/rules';

// loop over all rules
for (let rule in rules) {
  // add the rule
  extend(rule, rules[rule]);
}

Vue.component('ValidationProvider', ValidationProvider)
Vue.component('ValidationObserver', ValidationObserver)
